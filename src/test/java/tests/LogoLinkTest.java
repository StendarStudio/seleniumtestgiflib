package tests;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;
import page.objects.TopPage;
import static org.testng.Assert.assertEquals;

public class LogoLinkTest extends TestBase {

    private Logger logger = LogManager.getRootLogger();

    @Test

    public void asUserClickOnLogo() {

        TopPage topPage = new TopPage();
        topPage.clickOnLogoLink();
        logger.info("New Site");
        String currentUrl = DriverManager.getWebDriver().getCurrentUrl();
        logger.info("Get current url");
        assertEquals("https://lit-spire-41925.herokuapp.com/", currentUrl);
        logger.info("Back to the Home Page");
    }
}
