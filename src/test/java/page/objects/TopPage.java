package page.objects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class TopPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(className = "brand-logo")
    private WebElement brandLogo;

    public TopPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public TopPage clickOnLogoLink() {
        WaitForElement.waitUntilElementIsVisible(brandLogo);
        brandLogo.click();
        logger.info("Click on Logo");
        return new TopPage();

    }

}
